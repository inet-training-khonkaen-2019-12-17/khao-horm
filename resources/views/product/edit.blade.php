@extends('layouts.app')
​
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Product Edit Form <strong> # {{ $product->name }}</strong> </div>
​
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
​
                        <form action="{{ route('product.edit') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $product->id }}" name="id">
                            <div class="form-group">
                                <label for="name">Name :</label>
                                <input type="text" name="name" value="{{ $product->name }}" class="form-control" id="name" required>
                            </div>
                            <div class="form-group">
                                <label for="Price">Price :</label>
                                <input type="text" name="price" value="{{ $product->price }}" class="form-control" id="price" required>
                            </div>
                            <div class="form-group">
                                <label for="author">Url Image :</label>
                                <input type="file" name="url_image" value="{{ $product->url_image }}" class="form-control" id="url_image" required>
                            </div>
                            <div align="right">
                                <a href="{{ route('product.page') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
​
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
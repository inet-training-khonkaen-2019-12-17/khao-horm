<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><b><h2>Product list</h2></b></center></div> <br> 
                  <div align="right" style="margin-bottom: 30px">
                    <a href="/product/create" class="btn badge-secondary">+ Product</a>
                  </div>
                    <table class="table">
                      <thead class="thead-dark" >
                        <tr>
                          <th scope="col-lg-6">No.</th>
                          <th scope="col-lg-6">Name</th>
                          <th scope="col-lg-6">Price</th>
                          <th scope="col-lg-6">Image</th>
                          <th scope="col-lg-6">Edit</th>
                          <th scope="col-lg-6">Delete</th>
                        </tr>
                      </thead>
                        @foreach ($products as $product)
                      <tbody>
                        <tr>
                          <td>{{ $product->id }}</td>
                          <td width="200">{{ $product->name }}</td>
                          <td>{{ $product->price }}</td>
                          <td><img src="{{ $product->url_image }}" width="150"></td>
                          <td><a href="{{ route('product.edit.page', $product->id)}}" type="button" class="btn btn-primary"><i class="material-icons">&#xe254;</i></a></td>
                          <td><a href="{{ route('product.delete', $product->id) }}" type="button" class="btn btn-danger"><i class="material-icons">&#xe92b;</i></a></td>
                          @endforeach
                        </tr>
                      </tbody>
                    <table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



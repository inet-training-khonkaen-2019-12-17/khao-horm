<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        

        <!-- Fonts -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Styles -->
        <style>
        
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 150vh;
                margin: 0;
            }
            .bg {
  /* The image used */
            background-image: url("https://images.pexels.com/photos/1048023/pexels-photo-1048023.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
           

            /* Half height */
            height: 100%;
            
          /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
            }
            .position {
              left: 350px;
              top: 50px;
              height: 60vh;
            }

            .mdb-color-darker-hover {
            color: #59698D;
            -webkit-transition: .4s;
            transition: .4s; }
            .cyan-lighter-hover:hover {
              -webkit-transition: .4s;
              transition: .4s;
              color: #22e6ff; }
/* 
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            } */

        </style>
    </head>
    <body>

        <div class="bg">

        <div class="container">
        <div class= "col">
        
        <div class="card position bg-dark" style="max-width: 35rem;">

        <div class="card mb-3">
        <img src="https://i.ibb.co/85tzj3H/KHAO-HORM.jpg" class="card-img-top" style="max-width: 35rem;">
        <div class="card-body bg-dark">

        <form class="bg-dark" action="{{route('register')}}" method="post">
                            @csrf
                            <div class="form-group text-white">
                                <label for="name">Username</label>
                                <input type="text" name="username" class="form-control" id="name" required>
                            </div>
                            <div class="form-group text-white">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" name="password" class="form-control" id="exampleInputPassword1" required>
                            </div>
                            <div class="form-group text-white">
                                <label for="exampleInputConfirmPassword1">Confirm Password</label>
                                <input type="password" name="password_confirmation" class="form-control" id="exampleInputConfirmPassword1" required>
                            </div>
                            <div class="form-group text-white">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group text-white">
                                <label for="exampleInputEmail1">Tel</label>
                                <input type="text" name="tel" class="form-control" id="tel" aria-describedby="emailHelp" required>
                                <small id="tel" class="form-text text-muted">Example : xxxxxxxxxx</small>
                            </div>
                           
                            <button type="submit" class="btn btn-primary">Register</button>
                        </form>

      </div>
    </div>
        </div>
        </div>        
        </div>
        </body>
</html>

<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $products = [
        [
          'name' => 'สเต็กไก่ใหม่สดทุกวัน',
          'price' => 129,
          'url_image' => 'https://i.ibb.co/Q7gNT33/01.png',     
        ],
        [
          'name' => 'สเต็กหมูอู๊ดดดๆ',
          'price' => 159,
          'url_image' => 'https://i.ibb.co/bQfJ7f9/02.png',     
        ],
        [
          'name' => 'สเต็กเนื้อกินละติดเชื้อแน่นอน',
          'price' => 229,
          'url_image' => 'https://i.ibb.co/P4QtNxV/03.jpg',     
        ],
        [
          'name' => 'สเต็กเนื้อปลาแซลมอน',
          'price' => 269,
          'url_image' => 'https://i.ibb.co/QdMGYH7/04.png',     
        ],
        [
          'name' => 'ซุบเห็ดเด็ดทุกคำ',
          'price' => 129,
          'url_image' => 'https://i.ibb.co/LtFZHry/05.png',     
        ],
        [
          'name' => 'สปาเก็ตตี้',
          'price' => 129,
          'url_image' => 'https://i.ibb.co/Tv2pjPC/06.png',     
        ],
        [
          'name' => 'ชุดอาหารเช้าสุดคุ้ม',
          'price' => 269,
          'url_image' => 'https://i.ibb.co/TTKNbZx/07.jpg',     
        ],
        [
          'name' => 'อาหารอินตาลาเดีย',
          'price' => 269,
          'url_image' => 'https://i.ibb.co/dp30Ft2/08.png',     
        ],
        [
          'name' => 'มักกะโรนี',
          'price' => 129,
          'url_image' => 'https://i.ibb.co/wcTt6tk/09.png',     
        ],
        [
          'name' => 'ขนมจีบ',
          'price' => 129,
          'url_image' => 'https://i.ibb.co/dcSmGt1/10.jpg',     
        ],[
          'name' => 'ข้าวกะเพราหมูตู้วหู้ว',
          'price' => 269,
          'url_image' => 'https://i.ibb.co/TYdnMbL/11.png',     
        ]
        ];

      DB::table('products')->insert($products);
    }
}
